using System.Collections;
using System.Collections.Generic;
using Mosaic.Base;
using Template.Core;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Mosaic {
    public class MenuScreen : BaseScreen {
        public const string Exit_Game = "Exit_Game";

        [SerializeField] private LevelsGrid levelsGrid; //+
       // [SerializeField] private TextMeshProUGUI scoresText;
        public override void Show() {
            base.Show();
           // scoresText.text = "Scores: " + GameInfo.Instance.Scores.ToString();
            levelsGrid.LevelSelected += OnLevelSelected;
            
            /*var  _levelStates = new List<LevelState>();
            for (int i = 0; i < GameInfo.Instance.LevelsConfigs.Count; i++) {
                var _levelState = GameInfo.Instance.GetLevelState(i);
                if (_levelState == LevelState.NeedUnlock) {
                    _levelState = LevelState.Unlocked;
                    GameInfo.Instance.SetLevelState(i, _levelState);
                }
                _levelStates.Add(_levelState);
            }
            levelsGrid.ShowLevels(GameInfo.Instance.LevelsConfigs, _levelStates);*/
            levelsGrid.ShowLevels();
        }
        
        public void OnLevelSelected(int _levelIndex) {
            GameInfo.Instance.LevelIndex = _levelIndex;
            Exit(Exit_Game);
        }
    }
}


