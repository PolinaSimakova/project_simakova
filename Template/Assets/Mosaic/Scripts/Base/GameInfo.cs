using System;
using System.Collections;
using System.Collections.Generic;
using Template.Core;
using UnityEngine;

namespace Mosaic.Base {
    public class GameInfo : BaseManager<GameInfo> {
      // [SerializeField] private List<LevelConfig> levelConfigs;//+

        public void Setup() {
            if (GetLevelState(0) == LevelState.Locked) {
                SetLevelState(0, LevelState.Unlocked);
            }
        }
        
        public LevelState GetLevelState(int _levelIndex) {
            return (LevelState)PlayerPrefs.GetInt(PrefsKeys.Level_ + _levelIndex);
        }

        public void SetLevelState(int _levelIndex, LevelState _levelState) {
            PlayerPrefs.SetInt(PrefsKeys.Level_ + _levelIndex, (int)_levelState);
        }

       //public List<LevelConfig> LevelsConfigs => levelConfigs;//+
       
        public int LevelIndex { get; set; }

    }
    
}

