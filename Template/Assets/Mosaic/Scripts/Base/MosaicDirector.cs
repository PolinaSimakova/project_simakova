using System;
using System.Collections;
using System.Collections.Generic;
using Template.Core;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Mosaic.Base
{
    public class MosaicDirector : AppDirector
    {
        private void Start() {
            GameInfo.Instance.Setup();
            SceneManager.LoadScene("Menu");
        }
    }
}
