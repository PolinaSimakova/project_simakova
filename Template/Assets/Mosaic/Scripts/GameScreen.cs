using System.Collections;
using System.Collections.Generic;
using Mosaic.Base;
using Template.Core;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Mosaic {
    public class GameScreen : BaseScreen {
        public const string Exit_Back = "Exit_Back";
        public const string Exit_EndGame = "Exit_EndGame";
        
        //private LevelConfig levelConfig;
        
        [SerializeField] private MosaicController mosaicController;
        public void ShowAndStartGame() {
            Show();
            string imageName = Context.imageName;
            for (int i = 1; i <= 36; i++) {
                GameObject.Find("Piece_" + i).transform.Find("Image").GetComponent<SpriteRenderer>().sprite =
                    Resources.Load<Sprite>("Sprites/" + imageName);
            }
            int b = 1;
            // levelConfig = GameInfo.Instance.LevelConfig;

        }
        public void OnBackPressed() {
            Exit(Exit_Back);
        }
    }
}
