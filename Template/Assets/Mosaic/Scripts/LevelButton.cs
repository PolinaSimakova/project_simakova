using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Mosaic
{
    public class LevelButton : MonoBehaviour {
       // [SerializeField] private TextMeshProUGUI levelText; //+
        private int levelIndex; //+

        private Button button;
        
       // [SerializeField] private GameObject start;
       
        private void Awake() {
            button = GetComponent<Button>();
        }

        public void Setup(int _levelIndex) {//, LevelState _levelState
            levelIndex = _levelIndex; //+
          //  levelText.text = (_levelIndex + 1).ToString() + " Level"; //+
          //button.interactable = _levelState == LevelState.Unlocked;
        }

        public void OnButtonPressed() { //+
            LevelPressed.Invoke(levelIndex);
        }
        
        public Action<int> LevelPressed { get; set; } //+
        
        public void SetPuzzleImage(Image _image) {
            Context.imageName = _image.sprite.name;

            /*for (int i = 1; i <= 36; i++) {
                GameObject.Find("Piece_" + i).transform.Find("Image").GetComponent<SpriteRenderer>().sprite = _image.sprite;
            }*/
            //      start.SetActive(false);
        }
    }
}
