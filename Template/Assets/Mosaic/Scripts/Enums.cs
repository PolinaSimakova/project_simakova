using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Mosaic
{
    public enum LevelState {
        Locked,
        NeedUnlock,
        Unlocked
    }
}
