using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Mosaic
{
    [ExecuteAlways]
    public class CameraAspect : MonoBehaviour {
        [SerializeField] private float width;
        private Camera targetCamera;
        private void Awake() {
            targetCamera = GetComponent<Camera>();
        }

        private void Update() {
            targetCamera.orthographicSize = (float)Screen.height / Screen.width * width;
        }
    }
}
