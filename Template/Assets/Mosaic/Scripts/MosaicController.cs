using System;
using System.Collections;
using System.Collections.Generic;
using Mosaic.Base;
using UnityEngine;

namespace Mosaic
{
    public class MosaicController : MonoBehaviour {
        [SerializeField] private GameObject normalTileContentPrefab;
        [SerializeField] private List<NormalContentConfig> normalContentConfigs;
        
        void OnSwapContentsMoveEnd() {
            
        }
    }

    [Serializable]
    public class NormalContentConfig {
        [SerializeField] private Sprite sprite;

        public Sprite Sprite => sprite;
    }
}
