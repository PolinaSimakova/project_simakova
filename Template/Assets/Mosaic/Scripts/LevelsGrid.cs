using System;
using System.Collections;
using System.Collections.Generic;
using Mosaic.Base;
using UnityEngine;
using UnityEngine.UI;

namespace Mosaic
{
    public class LevelsGrid : MonoBehaviour {
        [SerializeField] private GameObject levelButtonPrefab;//+
        
        [SerializeField] private GameObject imagesList;//+
        public void ShowLevels() { //+List<LevelConfig> _levelsConfigs, List<LevelState> _levelStates
            for (int i = 0; i < 6; i++) {
                var _levelButton = Instantiate(levelButtonPrefab, transform);
                _levelButton.GetComponent<LevelButton>().Setup(i);
                
                var sprite = imagesList.GetComponent<Images>().images[i];
                _levelButton.GetComponent<Button>().image.sprite = sprite;
                
                _levelButton.GetComponent<LevelButton>().LevelPressed += OnLevelSelected; //+
            }
        }

        public void OnLevelSelected(int _levelIndex) { //+
            Debug.Log("OnLevelSelected: " + _levelIndex);
            LevelSelected.Invoke(_levelIndex);
        }
        
        public Action<int> LevelSelected { get; set; }
    }
}
