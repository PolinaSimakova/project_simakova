using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Image = UnityEngine.UIElements.Image;

namespace Mosaic
{
    public class Images : MonoBehaviour {
        public List<Sprite> images;
    }
}
