using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

namespace Mosaic
{
    public class DragAndDrop : MonoBehaviour
    {
        [SerializeField]private GameObject piece;
        private int layerPiece = 1;
        [SerializeField] private Camera camera;
        void Update()
        {
            if (Input.GetMouseButtonDown(0)) {
                RaycastHit2D hit = Physics2D.Raycast(camera.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);
                if (hit.transform.CompareTag("Puzzle")) {
                    if (!hit.transform.GetComponent<Piece>().IsPosition) {
                        piece = hit.transform.gameObject;
                        piece.GetComponent<Piece>().selectedPiece = true;
                        piece.GetComponent<SortingGroup>().sortingOrder = layerPiece;
                        layerPiece++;
                    }
                }
            }

            if (Input.GetMouseButtonUp(0)) {
                if (piece != null) {
                    piece.GetComponent<Piece>().selectedPiece = false;
                    piece = null;  
                }
            }
            if (piece != null) {
                Vector3 mousePoint = camera.ScreenToWorldPoint(Input.mousePosition);
                piece.transform.position = new Vector3(mousePoint.x, mousePoint.y, 0);
            }
        }
    }
}
