using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

namespace Mosaic
{
    public class Piece : MonoBehaviour
    {
        private Vector3 position;
        public bool IsPosition;
        public bool selectedPiece;
        
        void Start() {
            position = transform.position;
            transform.position = new Vector3(Random.Range(-2f, 2.2f), Random.Range(-1f, -4f));
        }

        void Update() {
            if (Vector3.Distance(transform.position, position) < 0.5f) {
                if (!selectedPiece) {
                    if (IsPosition == false) {
                        transform.position = position;
                        IsPosition = true;
                        GetComponent<SortingGroup>().sortingOrder = 0;  
                    }
                }
            }
        }
    }
}
