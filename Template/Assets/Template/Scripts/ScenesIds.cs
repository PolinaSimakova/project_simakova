using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Template
{
    public static class ScenesIds {
        public const string Menu = "Menu";
        public const string Game = "Game";
    }
}
