using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Template.Core {
    public abstract class BaseManager : MonoBehaviour {
        protected void Awake() {
            
        }
    }
    public class BaseManager<T> : BaseManager where T : BaseManager
    {
        protected void Awake() {
            if (Instance) {
                throw new  SystemException("Instance must be null");
            }
            Instance = this as T;
        }
        
        public static T Instance { get; private set; }
    }

}
