using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Template.Core
{
    public abstract class SceneDirector : MonoBehaviour {
        private Dictionary<Type, BaseScreen> screensDict;
        private Stack<BaseScreen> screensStack;

        protected abstract void OnScreenExit(Type _screenType, string _exitCode);

        protected virtual void Start() {
            screensDict = new Dictionary<Type, BaseScreen>();
            screensStack = new Stack<BaseScreen>();
            
            for (int i = 0; i < transform.childCount; i++) {
                var _screen = transform.GetChild(i).GetComponent<BaseScreen>();
                if (_screen) {
                    
                    if (_screen.IsShow)
                        throw new Exception($"{_screen.name} must be disabled.");
                    screensDict.Add(_screen.GetType(), _screen);
                    _screen.Init(OnScreenExit);
                }
            }
        }

        protected T SetCurrentScreen<T>() where T : BaseScreen {
            BaseScreen _nextScreen = screensDict[typeof(T)];

            if (CurrentScreen) {
                CurrentScreen.Hide();

                if (BackScreen == _nextScreen) {
                    screensStack.Pop();
                }
                else {
                    screensStack.Push(CurrentScreen);
                }
            }

            CurrentScreen = _nextScreen;
            return CurrentScreen as T;
        }

        protected void ToBackScreen() {
            var _nextScreen = screensStack.Pop();
            CurrentScreen.Hide();
            CurrentScreen = _nextScreen;
            CurrentScreen.Show();
        }
        
        protected BaseScreen CurrentScreen { get; private set; }
        protected BaseScreen BackScreen => screensStack.Count > 0 ? screensStack.Peek() : null;

    }
}
